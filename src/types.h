/*
    This file is part of KFileMetaData
    SPDX-FileCopyrightText: 2014 Vishesh Handa <me@vhanda.in>

    SPDX-License-Identifier: LGPL-2.1-or-later
*/

#ifndef KFILEMETADATA_TYPES
#define KFILEMETADATA_TYPES

#include <QVariant>

namespace KFileMetaData {
namespace Type {

/**
 * A Type represents a way to represent a way to group files based on
 * a higher level view, which the user generally expects.
 *
 * Every extractor provides a list of types applicable for each file.
 *
 * https://www.iana.org/assignments/media-types/media-types.xhtml
 */
enum Type {
    FirstType = 0,
    Empty = 0,

    Application,

    Audio,

    Font,

    Example,

    Image,

    Message,

    Model,

    Multipart,

    Text,

    Video,

    LastType = Video
};

} // namespace Type
} // namespace KFileMetaData

Q_DECLARE_METATYPE(KFileMetaData::Type::Type)

#endif
